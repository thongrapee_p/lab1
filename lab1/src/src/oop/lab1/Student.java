package src.oop.lab1;

//TODO Write class Javadoc
/**
 * Student object that inherits from Person object 
 * Student contains name and id
 * 
 * @author cpe-ku
 *
 */
public class Student extends Person {
	private long id;

	/**
	 * Initialize a new Student object
	 * @param name is name of the new student
	 * @param id is the identification number of the new student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	/**
	 * Compare student's by name and id.
	 * They are equal if the name and id matches.
	 * @param obj is another Object to be compared to this object
	 * @return true if the name and id are the same, otherwise false
	 */
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != this.getClass())
			return false;

		Student other = (Student) obj;

		if (id == other.id)
			return true;

		return false;
	}
}
